

;;;code:

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))

(column-number-mode)
(show-paren-mode)
(electric-pair-mode)
(global-hl-line-mode t)
(ido-mode t)
(cua-mode t)

(windmove-default-keybindings)




;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#292d3e" "#f07178" "#c3e88d" "#ffcb6b" "#82aaff" "#c792ea" "#82aaff" "#959dcb"])
 '(ansi-term-color-vector
   [unspecified "#292d3e" "#f07178" "#c3e88d" "#ffcb6b" "#82aaff" "#c792ea" "#82aaff" "#959dcb"])
 '(cua-mode t nil (cua-base))
 '(custom-enabled-themes (quote (graphene-meta)))
 '(custom-safe-themes
   (quote
    ("756ec68798410a2e705dd719c7328af9ecbb782c94130d489b6b3109841833eb" "80930c775cef2a97f2305bae6737a1c736079fdcc62a6fdf7b55de669fbbcd13" "9be1d34d961a40d94ef94d0d08a364c3d27201f3c98c9d38e36f10588469ea57" "7d3ee5cee22625af0a2acd2349242f5c1951f481d0f32c43afab45dd0c92477a" default)))
 '(ido-enable-flex-matching t)
 '(package-selected-packages
   (quote
    (magit elfeed-org org swiper nginx-mode elfeed-goodies elfeed mbo70s-theme base16-theme use-package try treemacs-projectile ibuffer-projectile ibuffer-git i3wm graphene counsel)))
 '(scroll-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'graphene)
(require 'nginx-mode)

(global-set-key (kbd "C-x w") 'elfeed)
(global-set-key (kbd "C-x á") 'forward-list)
(global-set-key (kbd "C-x é") 'backward-list)

(setq elfeed-feeds
      '(("https://444.hu/feed" hir politika)
        ("http://hvg.hu/rss" hir politika)
        ("https://javascriptkicks.com/feeds/rss" javascript prog)))


(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)
(global-set-key "\C-s" 'swiper)


(global-set-key (kbd "C-x g") 'magit-status)
