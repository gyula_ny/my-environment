"use strict";

const winston = require("winston");
const path = require("path");
const filename = path.join(__dirname, "i3bar.log");
const growl = require("notify-send");
const { spawn } = require("child_process");
const ndjson = require("ndjson");
const { Transform } = require("stream");
const counters = {};
const blockcache = {};
let memleaks = 0;

const { makeWindowChangeHandler } = require("./widgetsHelpers.js");

const {
	getKeyboardLayout,
	getSeparatorBlock,
	getExternalIPBlock,
	getDateBlock,
	getTimeBlock,
	getHDDblock,
	getWeatherBlock,
	getActiveWindowTitle,
	getFreeSytemMemory,
	getLocalIpAddr,
	getIfVPNActive
} = require("./widgets");

winston.configure({
	transports: [
		new winston.transports.File({
			filename: filename,
			prettyPrint: true,
			level: "debug"
		})
	]
});

winston.info("starting up nodebar...");

const fixi3InfiniteJSON = new Transform({
	transform: function(chunk, encoding, cb) {
		this.push(chunk.toString().substring(1));
		cb();
	}
});

process.stdin
	.pipe(fixi3InfiniteJSON)
	.pipe(ndjson.parse())
	.on("data", json => {
		winston.info("Got click event: ", json);
	});

const i3 = require("i3").createClient();
const memwatch = require("memwatch-next");

// workspace events: event.current.num

const focus = {
	windowName: "",
	mark: false
};

const globalTimeTracking = new Map();
const windowChangeHandler = makeWindowChangeHandler(focus, globalTimeTracking);
i3.on("window", windowChangeHandler);

memwatch.on("leak", info => {
	memleaks++;
	winston.log("error", "Memory leak detectd", info);
	growl.notify(`Memory leak detected - ${memleaks}`, JSON.stringify(info));
	if (memleaks === 5) {
		winston.log("error", "5th memleak alert - exiting process");
		process.exit(1);
	}
});

/**
 * @param  {number} x - number of seconds to refresh block
 * @param  {function} fn - function to generate the block content
 */

const block = async function(x, fn) {
	if (typeof counters[fn] !== "number") {
		counters[fn] = 0;
	} else {
		counters[fn]++;
	}

	if (counters[fn] === x) {
		counters[fn] = 0;
	}

	if (counters[fn] === 0) {
		blockcache[fn] = await fn();
	}

	return blockcache[fn];
};

let alterLine = null;
const journal = spawn("journalctl", ["--no-pager", "-f", "-n 1", "-o", "json"]);
journal.stdout.pipe(ndjson.parse()).on("data", obj => {
	//     let str = JSON.stringify(obj);
	winston.log("info", obj);
	alterLine = [
		{
			full_text: "[Log entry]:",
			color: "#f2777a",
			border_bottom: 0,
			border_left: 0,
			border_right: 0,
			border_top: 0
		},
		{
			full_text: "  " + obj.MESSAGE + "  ",
			color: "#ffffff",
			border_bottom: 0,
			border_left: 0,
			border_right: 0,
			border_top: 0
		}
	];
	setTimeout(() => {
		alterLine = null;
	}, 3000);
});

const getLine = async () => {
	let normalLine = [
		await block(1, getActiveWindowTitle.bind(undefined, focus)),
		await block(60, getSeparatorBlock),
		await block(15 * 60, getWeatherBlock),
		await block(60, getSeparatorBlock),
		await block(5 * 60, getExternalIPBlock),
		await block(60, getSeparatorBlock),
		await block(5, getHDDblock),
		await block(60, getSeparatorBlock),
		await block(10, getLocalIpAddr),
		await block(60, getSeparatorBlock),
		await block(10, getIfVPNActive),
		await block(60, getSeparatorBlock),
		await block(10, getFreeSytemMemory),
		await block(60, getSeparatorBlock),
		await block(5, getKeyboardLayout),
		await block(60, getSeparatorBlock),
		await block(60, getDateBlock),
		await block(60, getSeparatorBlock),
		await block(1, getTimeBlock)
	];

	return alterLine ? alterLine : normalLine;
};

console.log('{ "version": 1, "click_events": true }');
console.log("[");

setInterval(() => {
	getLine()
		.then(line => console.log(JSON.stringify(line) + ","))
		.catch(error => console.log(error.message));
}, 1000);
