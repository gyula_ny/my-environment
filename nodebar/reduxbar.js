// reduxbar 


var redux = require('redux');
var createStore = redux.createStore;

const initialState = {

	show: [],
	blocks: {}
};

function reducer( state = initialState, action) {

	switch (action.type) {

		case "ADD_NEW_BLOCK":
			return Object.assign({}, state, { show: [ ...state.show, action.payload.name] }, 
				{ blocks: Object.assign({}, state.blocks, {[action.payload.name]: action.payload.block}) })

		case "UPDATE_TIME_BLOCK":
			return Object.assign({}, state, {
				blocks: Object.assign({}, state.blocks, { "time": action.payload.block} )
			})

	}

	return state;
}

// action creator function
// CAN have side-effects, be async etc.
function addNewBlock(name, fulltext, color) {
	return {
		type: 'ADD_NEW_BLOCK',
		payload: {
			name: name,
			block: {
				full_text: fulltext,
				color: color
			}
		}
	};
}

function updateBlock(name, icon, fn, color) {

}

function updateTimeBlock(color) {
	return {
		type: "UPDATE_TIME_BLOCK",
		payload: {
			name: "time",
			block: {
				full_text: "   " + new Date().toLocaleString(),
				color: color
			}
		}
	};
}


const store = createStore(reducer);

store.subscribe(function() {
	//render statusline:
	console.log(JSON.stringify(store.getState()));
});


setTimeout( ()=>{store.dispatch(addNewBlock("static", "hey, this is static text", "#cccccc"))}, 3000);

setInterval( ()=>{store.dispatch(updateTimeBlock("#dddddd"))}, 1000);

// console.log(store.getState());

// function reducer(state = [], action) {
// 	switch(action.type) {
// 		case 'ADD_BLOCK':
// 		..
// 		case 'UPDATE_BLOCK'


// 	}

// 	return state;
// }