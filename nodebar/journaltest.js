
var    spawn = require('child_process').spawn;
var    journalctl    = spawn('journalctl', ['-f'], {shell: true});

journalctl.stdout.on('data', function(data) {
	console.log(data.toString());
});

journalctl.stderr.on('data', function (data) {
  console.log('stderr: ' + data);
});

journalctl.on('exit', function (code) {
  console.log('child process exited with code ' + code);
});



