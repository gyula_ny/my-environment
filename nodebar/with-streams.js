"use strict";

const AsyncEventEmitter = require('async-eventemitter');
const events = new AsyncEventEmitter();

const DarkSky = require('dark-sky');
const apiKey = require('./APIKEY.json');
const forecast = new DarkSky(apiKey.APIKEY);

const updateWeather = function() {
  return forecast
    .latitude(47.4979)
    .longitude(19.0402)
    .units('si')
    .language('en')
    .get()
    .then(res => res.currently.summary + " (" + res.currently.temperature + " °C)"
    ).catch(e => { 
		console.error(e); 
		process.exit(1);
    });
};


events.on('test', function(e, next) {
	console.log("first event in test");
	console.log("received in param 'e': ", e);
	console.log("also adding a new key...");
	e.isitszopacs = true;
	e.original = 2;

	updateWeather().then(function(ret) {
		console.log(ret);
		next();
	});

});

events.on('test', function(e, next) {
	console.log("received in param 'e': ", e);
	console.log("second event handler - befor or after weather forecast?");
	next();
});

events.emit('test', { original: 1 }, err => {
	if (err) {
		console.error(err);
	}
	console.log("All listeners should have had run by now");
});
