"use strict";

const DarkSky = require("dark-sky");
const apiKey = require("./APIKEY.json");
const forecast = new DarkSky(apiKey.APIKEY);
const winston = require("winston");
const got = require("got");
const geoip2 = require("geoip2");
const fs = require("fs");
const path = require("path");
const timeTrackingFile = path.join(__dirname, "tracking.log");

geoip2.init();

module.exports = {
	updateWeather: function() {
		return forecast
			.latitude(47.4979)
			.longitude(19.0402)
			.units("si")
			.language("en")
			.get()
			.then(
				res =>
					res.currently.summary +
					" (" +
					Number(res.currently.temperature).toFixed(1) +
					" °C)"
			)
			.catch(e => {
				console.error(e);
				winston.log("error", "Error with weather widget: ", e);
			});
	},

	makeWindowChangeHandler: function(focus, globalTimeTracking) {
		let serialized;

		return function(ev) {
			focus.windowName = ev.container.name;
			focus.mark = true;

			winston.log(
				"info",
				"window change event. New window: ",
				focus.windowName
			);

			let toc = Number(new Date());
			if (globalTimeTracking.has("__running__")) {
				let previous = globalTimeTracking.get("__running__");

				if (globalTimeTracking.has(previous)) {
					let oldContainer = globalTimeTracking.get(previous);
					let oldEntry = oldContainer[0];
					oldEntry.finish = toc;
					oldEntry.finished = true;
					oldEntry.period = oldEntry.finish - oldEntry.start;
					winston.log("info", "closing: " + previous, oldEntry);
					oldContainer[0] = oldEntry;
					globalTimeTracking.set(previous, oldContainer);
				}
			}

			if (!globalTimeTracking.has(focus.windowName)) {
				winston.log("info", "start a new bucket");

				let newEntry = {
					start: toc,
					finished: false,
					finish: null,
					period: null
				};

				let newContainer = [newEntry];
				globalTimeTracking.set(focus.windowName, newContainer);
				winston.log(
					"info",
					"The bucket looks like this: ",
					newContainer
				);
			} else {
				winston.log(
					"info",
					"We DO have a bucket already for ",
					focus.windowName
				);

				let newEntry = {
					start: toc,
					finished: false,
					finish: null,
					period: null
				};

				winston.log(
					"info",
					"adding new entry to existing bucket: ",
					newEntry
				);

				let container = globalTimeTracking.get(focus.windowName);
				let newContainer = [newEntry].concat(container);
				globalTimeTracking.set(focus.windowName, newContainer);
			}

			winston.info("Setting __running__ state to ", focus.windowName);
			globalTimeTracking.set("__running__", focus.windowName);

			try {
				serialized = [...globalTimeTracking];
			} catch (e) {
				winston.log(
					"info",
					"cannot serialize timetracking data: " + e.toString()
				);
			}

			if (serialized) {
				fs.writeFile(
					timeTrackingFile,
					JSON.stringify(serialized, null, 4),
					err => {
						if (err) {
							winston.log(
								"error",
								"cannot save serialization object: " + err
							);
						}
					}
				);
			}
		};
	},

	updateExternalIPBlock: async function() {
		let ip;
		try {
			const response = await got("http://httpbin.org/ip");
			ip = JSON.parse(response.body).origin;
			winston.log("info", "IP: ", ip);
		} catch (e) {
			winston.error("Error getting external IP: ", e);
			ip = null;
		}

		if (ip) {
			return new Promise((resolve, _) => {
				geoip2.lookupSimple(ip, (error, result) => {
					if (error) {
						resolve(ip + " (unknown) ");
					} else {
						resolve(ip + " (" + result.country + ") ");
					}
				});
			});
		} else {
			return Promise.reject(null);
		}
	},

	handleClickInput: function(fragment) {
		let jsonstring = fragment.toString("utf-8").replace("\n", "");

		if (jsonstring.startsWith(",")) {
			jsonstring = jsonstring.slice(1);
		}

		if (jsonstring.startsWith("[")) {
			jsonstring = jsonstring + "]";
		}

		let parsed;
		try {
			parsed = JSON.parse(jsonstring);
			winston.info("Got click event: ", parsed);
		} catch (e) {
			winston.error("Unable to parse: ", jsonstring);
		}
	}
};
