const { exec } = require("child_process");
const { updateWeather, updateExternalIPBlock } = require("./widgetsHelpers.js");
const os = require("os");
const disk = require("diskusage");

// TODO: lose this module
const D = require("date-fp");
const niceDate = D.format("MMMM D (dddd)");
const niceTime = D.format("h:mm:ss a");

const activeNetworkInterfaces = Object.keys(os.networkInterfaces()).filter(
	x => x !== "lo"
);

let currentKeyboardLayout = {
	name: "KeyboardLayout",
	full_text: " " + "getting layout" + " ",
	// color: colors.color0,
	border: "#85775a"
};

const updateKeyboardLayout = function() {
	exec('xkblayout-state print "%e"', (error, stdout, stderr) => {
		if (!error && !stderr) {
			currentKeyboardLayout.full_text = (
				" " +
				stdout +
				" "
			).toUpperCase();
		}
	});
};

const commonBlockElements = {
	separator: false,
	separator_block_width: 0,
	border_bottom: 0,
	border_left: 0,
	border_right: 0,
	border_top: 0,
	color: "#d3d0c8",
	markup: "pango"
};

const forSep = Object.assign({}, commonBlockElements, {
	border_top: 0
});

const _ = obj => Object.assign({}, commonBlockElements, obj);

const getKeyboardLayout = function() {
	updateKeyboardLayout();
	return _(currentKeyboardLayout);
};

// characters: " • • ∷ ▣ ➥ "
const getSeparatorBlock = function(character, color) {
	return Object.assign({}, forSep, {
		name: "separator",
		full_text: " ∷ ",
		color: "#8be9fd"
	});
};

const getExternalIPBlock = async function() {
	let externalIP;
	try {
		externalIP = await updateExternalIPBlock();
	} catch (e) {
		externalIP = "error getting eIP";
	}
	// updateExternalIPBlock();

	return {
		name: "ExternalIPAddress",
		full_text:
			" <span color='#ff5555'>IP: </span>" +
			(externalIP || "getting eIP") +
			" ",
		// color: colors.color0,

		border: "#b4a990",
		separator: false,
		separator_block_width: 0,
		border_bottom: 0,
		border_left: 0,
		border_right: 0,
		border_top: 0,
		color: "#d3d0c8",
		markup: "pango"
	};
};

const getDateBlock = function() {
	return _({
		name: "date",
		full_text: (" " + niceDate(new Date()) + " ").toUpperCase(),
		// color: colors.color0,
		border: "#796c52"
	});
};

const getTimeBlock = function() {
	return _({
		name: "time",
		full_text: " " + niceTime(new Date()) + " ",
		// color: colors.color0,
		border: "#ff5555",
		border_top: 2,
		// min_width: "XX:XX:XX pm"
		min_width: 90
	});
};

let hdd_full_text = "";

const getHDDblock = function() {
	disk.check("/home", (err, info) => {
		hdd_full_text =
			(info.available / (1024 * 1024 * 1014)).toFixed(2) + "GB";
	});

	return _({
		name: "disk",
		instance: "home",
		full_text:
			" <span color='#ff5555'>/home: </span>" +
			(" " + hdd_full_text + " ").toUpperCase(),
		// color: colors.color0,
		border: "#ac9f84"
	});
};

const getWeatherBlock = async function() {
	let weather;
	try {
		weather = await updateWeather();
	} catch (e) {
		weather = "(can't get no weather:( )";
	}

	return _({
		name: "weather",
		full_text: (" " + weather + " ").toUpperCase(),
		// color: colors.color0,
		border: "#bcb29c"
	});
};

const getActiveWindowTitle = function(focus) {
	return {
		name: "currentWindow",
		full_text:
			"<span font_weight='bold'> " +
			(focus.windowName || "not yet") +
			"</span> ",
		border_bottom: 0,
		border_left: 0,
		border_right: 0,
		border_top: 0,
		color: "#f8f8f8",
		markup: "pango"
	};
};

const getFreeSytemMemory = function() {
	let free = os.freemem();
	return _({
		name: "memory",
		full_text: " " + (free / (1024 * 1024 * 1024)).toFixed(2) + " GB ",
		// color: free < 3 ? colors.color13 : colors.color8
		// color: colors.color0,
		border: "#918262"
	});
};

const getLocalIpAddr = function() {
	// console.log("running!!!");
	return _({
		name: "network",
		instance: "localWiredIpv4",
		full_text:
			" <span color='#ff5555'>eno1: </span>" +
			os
				.networkInterfaces()
				[activeNetworkInterfaces[0]].filter(x => x.family === "IPv4")[0]
				.address +
			" ",
		//  .map(function(x) {
		//    return x.address;
		//  }),
		// color: colors.color0,
		border: "#a49678"
	});
};

const getIfVPNActive = function() {
	if (!("vpn0" in os.networkInterfaces())) {
		return _({
			name: "network",
			instance: "VPNIpv4",
			full_text: " VPN DOWN ",
			// color: colors.color0,
			border: "#9c8c6c"
		});
	} else {
		return _({
			name: "network",
			instance: "VPNIpv4",
			full_text:
				" VPN  " +
				os.networkInterfaces().vpn0.filter(x => x.family === "IPv4")[0]
					.address +
				" ",
			// color: colors.color0,
			border: "#9c8c6c"
		});
	}
};

module.exports = {
	getKeyboardLayout,
	getSeparatorBlock,
	getExternalIPBlock,
	getDateBlock,
	getTimeBlock,
	getHDDblock,
	getWeatherBlock,
	getActiveWindowTitle,
	getFreeSytemMemory,
	getLocalIpAddr,
	getIfVPNActive
};
