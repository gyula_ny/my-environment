var i3 = require('i3').createClient();

var windowHandle = function(ev) {
    console.log(ev.container.name);
}

var workspaceHandle = function(ev) {
    console.log("workspace event", ev);
}

var outputHandle = function() {
    console.log("output event", arguments);
}

i3.on("window", windowHandle);
i3.on("workspace", workspaceHandle);
i3.on("output", outputHandle);



